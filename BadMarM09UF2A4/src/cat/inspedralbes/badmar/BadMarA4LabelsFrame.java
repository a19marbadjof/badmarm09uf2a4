package cat.inspedralbes.badmar;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class BadMarA4LabelsFrame extends JFrame implements ActionListener {

	private JPanel contentPane;
	JPanel panel;
	Thread thread;
	ArrayList<BadMarA4MovingLabel> labels = new ArrayList<>();
	JButton btnAdd;
	JButton btnStop;
	Random rand = new Random();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BadMarA4LabelsFrame frame = new BadMarA4LabelsFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BadMarA4LabelsFrame() {
		setTitle("BadMarA4MovingLabels");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setBounds(0, 0, 428, 235);
		panel.setBackground(Color.WHITE);
		contentPane.add(panel);
		panel.setLayout(null);

		btnAdd = new JButton("ADD");
		btnAdd.addActionListener(this);
		btnAdd.setBounds(15, 240, 105, 25);
		contentPane.add(btnAdd);

		btnStop = new JButton("STOP");
		btnStop.setBounds(309, 240, 117, 25);
		btnStop.addActionListener(this);
		contentPane.add(btnStop);				
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String opcio = arg0.getActionCommand();
		switch (opcio) {
		case "ADD":
			labels.add(new BadMarA4MovingLabel(panel));
			thread = (new Thread(labels.get(labels.size() - 1)));
			thread.start();
			System.out.println(Thread.activeCount());
			break;

		case "STOP":
			System.out.println(Thread.activeCount());
			if(!labels.isEmpty()) {
				int num = rand.nextInt(labels.size());
				labels.get(num).stop();
				labels.remove(num);
			}
			
			break;

		}
	}
}
