package cat.inspedralbes.badmar;

import java.awt.Color;
import java.awt.Panel;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class BadMarA4MovingLabel extends JLabel implements Runnable {

	JPanel panel_1;
	int x = 0;
	int y = 0;
	boolean comprovar = false;

	public BadMarA4MovingLabel(JPanel panel_1) {
		this.panel_1 = panel_1;

		setBackground(Color.BLUE);
		setSize(10,10);
		setOpaque(true);
		setLocation(x, y);
		panel_1.add(this);
		panel_1.repaint();
		panel_1.revalidate();

	}

	@Override
	public void run() {

		do {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			x = x + 10;
			y = y + 10;
			setLocation(x, y);
			panel_1.repaint();
		} while ((y <= panel_1.getHeight()) && (!comprovar));

	}

	public void stop() {
		comprovar = true;
		setOpaque(false);
	}
}
